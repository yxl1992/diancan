//app.js
App({
  onLaunch: function () {
    // 展示本地存储能力
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)

    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
      }
    })
    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              // 可以将 res 发送给后台解码出 unionId
              this.globalData.userInfo = res.userInfo

              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (this.userInfoReadyCallback) {
                this.userInfoReadyCallback(res)
              }
            }
          })
        }
      }
    })
  },
  globalData: {
    foodList:[
      {
        id: 1,
        title: '黑胡椒意大利面',
        cost: 32,
        desc: '口感好，味道佳',
        icon: 'http://m.tuniucdn.com/filebroker/cdn/vnd/7a/17/7a17c393cad280df72b098c0db48fb62_w500_h280_c1_t0.jpg',
        num: 0
      },
      {
        id: 2,
        title: '黑胡椒意大利面',
        cost: 32,
        desc: '口感好，味道佳',
        icon: 'http://images.517best.com/UploadFiles/images/s/20140919/170313697720140919112804365535742.jpg',
        num: 0
      },
      {
        id:3,
        title: '黑胡椒意大利面',
        cost: 32,
        desc: '口感好，味道佳',
        icon: 'http://img0.imgtn.bdimg.com/it/u=452765978,1840923288&fm=214&gp=0.jpg',
        num: 0
      },
      {
        id: 4,
        title: '黑胡椒意大利面',
        cost: 32,
        desc: '口感好，味道佳',
        icon: 'http://www.tucheng.cn/UploadFiles/sharefile/image/%B9%E3%B6%AB/%B3%B1%D6%DD/%B3%B1%D6%DD%D1%CE%BE%D6%BC%A6.jpg',
        num: 0
      },
      {
        id: 1,
        title: '黑胡椒意大利面',
        cost: 32,
        desc: '口感好，味道佳',
        icon: 'http://m.tuniucdn.com/filebroker/cdn/vnd/7a/17/7a17c393cad280df72b098c0db48fb62_w500_h280_c1_t0.jpg',
        num: 0
      },
      {
        id: 2,
        title: '黑胡椒意大利面',
        cost: 32,
        desc: '口感好，味道佳',
        icon: 'http://images.517best.com/UploadFiles/images/s/20140919/170313697720140919112804365535742.jpg',
        num: 0
      },
      {
        id: 3,
        title: '黑胡椒意大利面',
        cost: 32,
        desc: '口感好，味道佳',
        icon: 'http://img0.imgtn.bdimg.com/it/u=452765978,1840923288&fm=214&gp=0.jpg',
        num: 0
      },
      {
        id: 4,
        title: '黑胡椒意大利面',
        cost: 32,
        desc: '口感好，味道佳',
        icon: 'http://www.tucheng.cn/UploadFiles/sharefile/image/%B9%E3%B6%AB/%B3%B1%D6%DD/%B3%B1%D6%DD%D1%CE%BE%D6%BC%A6.jpg',
        num: 0
      }
    ]
  }
})