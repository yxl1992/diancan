//index.js
//获取应用实例
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgUrls: [
                'http://m.tuniucdn.com/filebroker/cdn/vnd/7a/17/7a17c393cad280df72b098c0db48fb62_w500_h280_c1_t0.jpg',
              'http://images.517best.com/UploadFiles/images/s/20140919/170313697720140919112804365535742.jpg',
         'http://img0.imgtn.bdimg.com/it/u=452765978,1840923288&fm=214&gp=0.jpg',
                                 'http://www.tucheng.cn/UploadFiles/sharefile/image/%B9%E3%B6%AB/%B3%B1%D6%DD/%B3%B1%D6%DD%D1%CE%BE%D6%BC%A6.jpg'
    ],
    indicatorDots: true,
    autoplay: true,
    interval: 5000,
    duration: 1000,
    foodTypes: ['全部菜品', '凉菜', '热菜', '面食'],
    foodTypesIndex: 0,
    rankTypes: ['综合排序', '热度', '价格', '好评'],
    rankTypesIndex: 0,
    foodList:[],
    testfood: {
      id: 1,
      title: '黑胡椒意大利面',
      cost: 32,
      desc: '口感好，味道佳',
      icon: '',
      num: 0
    },
    orderNum: 0,
    orderCost: 0,
    orderList: {},
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
   let orginData=app.globalData.foodList;
   let foodList=[];
   for(let i=0;i<orginData.length;i++){
     foodList.push(orginData[i]);

   }
   this.setData({foodList:foodList})


    
  },
  foodTypeChange(e) {
    console.log(e)
    this.setData({
      foodTypesIndex: e.detail.value
    })
  },
  rankTypeChange(e) {
    this.setData({
      rankTypesIndex: e.detail.value
    })
  },
  //增加数量
  addToCart :function(e){
    console.log(e);
    var dataset = e.currentTarget.dataset;
    this.changeNum(dataset.index,true)
    wx.showToast({
      title: '已添加',
      icon:'success',
      duration:1000,
    })

  },
  //减少数量
  reduceFromCart:function(e){
    var dataset = e.currentTarget.dataset;
    wx.showToast({
      title: '已删除',
      icon: 'success',
      duration: 1000,
    })
    this.changeNum(dataset.index, false);
  },
  changeNum:function(index,bool){
    let t_food=this.data.foodList[index];
    let orderList=this.data.orderList;
    let obj=orderList[t_food.id];
    //如果存在。数量变化
    if(obj){
      if(bool){
        obj.num=obj.num+1;
      }else{
        if(obj.num>0){
          obj.num = obj.num - 1;
        }else{
          return
        }
       
      }
    }else{
      if(bool){
        obj={
          id:t_food.id,
          num:1,
          cost:t_food.cost,
          title:t_food.title

        };
        orderList[t_food.id]=obj;

      }else{
        return;
      }
    }

    var order_num=0;
    var order_cost=0;
    for(let k  in orderList){
      order_num=orderList[k].num+order_num;
      order_cost=order_cost+orderList[k].num *orderList[k].cost;
    }
    t_food.num=obj.num;
    let  foodList=this.data.foodList;
    foodList[index]=t_food;
    this.setData({
      orderList:orderList,
      orderNum:order_num,
      orderCost:order_cost,
      foodList:foodList
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return{
      title:'点餐系统'
    }
  }
})
